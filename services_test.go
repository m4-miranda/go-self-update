package main

import (
	"os"
	"testing"
)

func TestGetLatestVersionService(t *testing.T) {
	wantExecutable := "www.google.com"
	wantSignature := "www.twitter.com"
	wantVersion := "1.9"

	testJSON := `
		{
			"version": "1.9",
			"executable": "www.google.com",
			"signature": "www.twitter.com"
		}
	`
	testFilePath := "test_file"
	testFile, _ := os.Create(testFilePath)
	testFile.WriteString(testJSON)
	testFile.Close()
	defer os.Remove(testFilePath)

	testResponse, err := GetLatestVersionService(testFilePath)
	if err != nil {
		t.Error(err)
	}

	gotExecutable := testResponse.ExecDownloadPath
	gotSignature := testResponse.SigDownloadPath
	gotVersion := testResponse.Version

	if gotExecutable != wantExecutable {
		t.Errorf("Got %s want %s", gotExecutable, wantExecutable)
	}
	if gotSignature != wantSignature {
		t.Errorf("Got %s want %s", gotSignature, wantSignature)
	}
	if gotVersion != wantVersion {
		t.Errorf("Got %s want %s", wantVersion, gotVersion)
	}

	t.Run("when one JSON key isn't present", func(t *testing.T) {
		testJSON = `{
			"version": "1.9",
			"executable": "www.google.com"
		}`
		testFile2Path := "test_file2"
		testFile2, _ := os.Create(testFile2Path)
		testFile2.WriteAt([]byte(testJSON), 0)
		testFile2.Close()
		defer os.Remove(testFile2Path)

		testResponse, err = GetLatestVersionService(testFile2Path)
		if err == nil {
			t.Errorf("Expected error ")
		}
	})
}
