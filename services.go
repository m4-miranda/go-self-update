package main

import (
	"encoding/json"
	"fmt"
	"io"
	"os"
	"path/filepath"
)

// GetLatestVersionURL contains the path to query to check for updates
const GetLatestVersionURL = "./mock_service/update.json"

// LatestVersionResponse Contains the parsed response values from web service which returns latest version number,
// executable & signature download path
type LatestVersionResponse struct {
	Version string `json:"version"`

	ExecDownloadPath string `json:"executable"`

	SigDownloadPath string `json:"signature"`
}

/*
CallUpdateService queries web-service at servicePath and returns
latest version string, update file & signature's path
For this application, read the contents of the file at servicePath and return it's
values stored (JSON)
*/
func GetLatestVersionService(webServicePath string) (response *LatestVersionResponse, err error) {
	filePath, err := filepath.Abs(webServicePath)
	if err != nil {
		err = fmt.Errorf("From filepath.Abs: %v", err)
		return
	}

	serviceFile, err := os.Open(filePath)
	if err != nil {
		err = fmt.Errorf("From os.Open(): %v", err)
		return
	}
	defer serviceFile.Close()

	responseBytes := make([]byte, 1000)
	bytesRead, err := serviceFile.Read(responseBytes)
	if err != nil {
		return nil, fmt.Errorf("From file.Read(): %v", err)
	}

	toReturn := &LatestVersionResponse{}

	err = json.Unmarshal(responseBytes[:bytesRead], toReturn)
	if err != nil {
		return nil, fmt.Errorf("From json.Unmarshal: %v", err)
	}

	if !toReturn.isValid() {
		return nil, fmt.Errorf("Invalid JSON structure from service")
	}

	return toReturn, nil
}

func (response *LatestVersionResponse) isValid() bool {
	if (response.ExecDownloadPath == "") || (response.SigDownloadPath == "") || (response.Version == "") {
		return false
	}
	return true
}

/*
DownloadFileService downloads the file from external location to local
directory. It renames the file to provided destination file name
In production app, this method would make an HTTP request to download the file
to local path
*/
func DownloadFileService(sourceFilePath, destFilePath string) (err error) {

	srcFile, err := os.Open(sourceFilePath)
	if err != nil {
		return err
	}
	defer srcFile.Close()

	dstFile, err := os.Create(destFilePath)
	if err != nil {
		return err
	}
	defer dstFile.Close()

	_, err = io.Copy(dstFile, srcFile)
	if err != nil {
		return err
	}

	return
}
