package main

import (
	"bytes"
	"crypto/x509"
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"os"
	"strings"
	"testing"
)

func TestLoadPublicKeyFrom(t *testing.T) {
	keyBytes, _ := hex.DecodeString("3082010a02820101009fed58993017f2ba7238ce618ae6690177d7c2a3be69b3c5385f65336e93899e2fef7ae61e2689ddf7a40c925cff1ac6566d6ec754da42eaff2f4d3ea87c45f75ef796d7dd61edff97bd2950c4dfd730e50527cf53edc0a93ae64a64d49db228be7604826b3c40bdf38a7955083f064f7345ee37049b83ada152a7eed69580295b4eace93be4b4583da01966ff35178961b288600af0c984a69e82d84541ce7db0710b1a486957504bf91ee301c38b54fc9d05ce60e0ee1ba9f9fe90cea532e78703fa272752a9318d05fb1739735347162ac153f4eae9229018accefb5f027349d9adf07efbaeba3257bf6441c0868d9a860dbf92128c77df019dcb942ef9110203010001")
	keyPEM := strings.NewReader("-----BEGIN RSA PUBLIC KEY-----\nMIIBCgKCAQEAn+1YmTAX8rpyOM5hiuZpAXfXwqO+abPFOF9lM26TiZ4v73rmHiaJ\n3fekDJJc/xrGVm1ux1TaQur/L00+qHxF9173ltfdYe3/l70pUMTf1zDlBSfPU+3A\nqTrmSmTUnbIovnYEgms8QL3zinlVCD8GT3NF7jcEm4OtoVKn7taVgClbTqzpO+S0\nWD2gGWb/NReJYbKIYArwyYSmnoLYRUHOfbBxCxpIaVdQS/ke4wHDi1T8nQXOYODu\nG6n5/pDOpTLnhwP6JydSqTGNBfsXOXNTRxYqwVP06ukikBiszvtfAnNJ2a3wfvuu\nujJXv2RBwIaNmoYNv5ISjHffAZ3LlC75EQIDAQAB\n-----END RSA PUBLIC KEY-----")

	t.Run("when key is valid", func(t *testing.T) {
		testPubKey, err := loadPublicKeyFrom(keyPEM)
		if err != nil {
			t.Errorf("Expect nil error, got: %v", err)
		}

		bTest := x509.MarshalPKCS1PublicKey(testPubKey)

		if !bytes.Equal(keyBytes, bTest) {
			t.Errorf("Expected PKCS1v15 bytes to be equal")
		}
	})
}

func TestLoadPublicKey(t *testing.T) {
	testFile, _ := os.Create("testing_file")
	defer os.Remove("testing_file")

	keyPEM := strings.NewReader("-----BEGIN RSA PUBLIC KEY-----\nMIIBCgKCAQEAn+1YmTAX8rpyOM5hiuZpAXfXwqO+abPFOF9lM26TiZ4v73rmHiaJ\n3fekDJJc/xrGVm1ux1TaQur/L00+qHxF9173ltfdYe3/l70pUMTf1zDlBSfPU+3A\nqTrmSmTUnbIovnYEgms8QL3zinlVCD8GT3NF7jcEm4OtoVKn7taVgClbTqzpO+S0\nWD2gGWb/NReJYbKIYArwyYSmnoLYRUHOfbBxCxpIaVdQS/ke4wHDi1T8nQXOYODu\nG6n5/pDOpTLnhwP6JydSqTGNBfsXOXNTRxYqwVP06ukikBiszvtfAnNJ2a3wfvuu\nujJXv2RBwIaNmoYNv5ISjHffAZ3LlC75EQIDAQAB\n-----END RSA PUBLIC KEY-----")

	keyPEM.WriteTo(testFile)
	testFile.Close()

	testUpdate := &Update{PubKeyPath: "testing_file"}

	if err := testUpdate.LoadPublicKey(); err != nil {
		t.Errorf(" %v", err)
	}

	if testUpdate.PublicKey == nil {
		t.Errorf("Expected PublicKey to be non-nil")
	}

}

func TestLoadFileHash(t *testing.T) {
	testFile, _ := os.Create("testing_file")
	defer os.Remove("testing_file")

	testFile.WriteString("hello")
	testFile.Close()

	testUpdate := &Update{execLocalPath: "testing_file"}

	if err := testUpdate.LoadFileHash(); err != nil {
		t.Error(err)
	}

	if testUpdate.execHashSum == nil {
		t.Errorf("Expected execHashSum to be non-nil")
	}
}

func TestDownloadFiles(t *testing.T) {
	testFile1, _ := os.Create("testing1")
	testFile2, _ := os.Create("testing2")

	testFile1.WriteString("hi")
	testFile2.WriteString("hello")

	testFile1.Close()
	testFile2.Close()

	defer os.Remove("testing1")
	defer os.Remove("testing2")

	testUpdate := &Update{execRemotePath: "testing1", sigRemotePath: "testing2", OriginalExecPath: "file"}

	if err := testUpdate.DownloadFiles(); err != nil {
		t.Error(err)
	}

	if testUpdate.execLocalPath == "" {
		t.Errorf("Expected execLocalPath to be set")
	}

	if testUpdate.sigLocalPath == "" {
		t.Errorf("Expected sigLocalPath to be set")
	}

	if err := os.Remove(testUpdate.sigLocalPath); os.IsNotExist(err) {
		t.Errorf("Expect file to exist")
	}

	if err := os.Remove(testUpdate.execLocalPath); os.IsNotExist(err) {
		t.Errorf("Expect file to exist")
	}

}

func TestLoadSignature(t *testing.T) {
	testSigFile, _ := os.Create("test_sig")

	want := []byte("Hi this is a long string to be encoded")

	signature := base64.StdEncoding.EncodeToString(want)
	testSigFile.WriteString(signature)

	testSigFile.Close()
	defer os.Remove(testSigFile.Name())

	testUpdate := &Update{sigLocalPath: "test_sig"}

	if err := testUpdate.LoadSignature(); err != nil {
		t.Error(err)
	}

	got := testUpdate.Signature

	if !bytes.Equal(got, want) {
		t.Errorf("Expected %v got %v", want, got)
	}

}

func TestCleanup(t *testing.T) {
	t.Run("if files were never downloaded", func(t *testing.T) {
		testUpdate := &Update{}

		if err := testUpdate.CleanUp(); err != nil {
			t.Error(err)
		}
	})

	t.Run("if only executable was downloaded", func(t *testing.T) {
		testUpdate := &Update{execLocalPath: "testing_file"}
		f, _ := os.Create("testing_file")
		f.Close()

		if err := testUpdate.CleanUp(); err != nil {
			t.Error(err)
		}

		if _, err := os.Stat("testing_file"); err == nil {
			t.Error("Expected file not to exist")
		}
	})

	t.Run("if both files were downloaded", func(t *testing.T) {
		testUpdate := &Update{execLocalPath: "testing_file1", sigLocalPath: "testing_file2"}
		f1, _ := os.Create("testing_file1")
		f2, _ := os.Create("testing_file2")
		f1.Close()
		f2.Close()

		if err := testUpdate.CleanUp(); err != nil {
			t.Error(err)
		}

		if _, err := os.Stat("testing_file1"); err == nil {
			t.Error("Expected file not to exist")
		}

		if _, err := os.Stat("testing_file2"); err == nil {
			t.Error("Expected file not to exist")
		}
	})
}

func TestPerformRenaming(t *testing.T) {
	firstUpdatePath := "test_file1"
	oldExecutablePath := "test_file2"

	firstUpdateFile, _ := os.Create(firstUpdatePath)
	oldExecutableFile, _ := os.Create(oldExecutablePath)

	// firstUpdateData := "New Executable"
	// oldExecutableData := "Old Executable"

	firstUpdateFile.WriteString("New Executable")
	oldExecutableFile.WriteString("Old Executable")

	firstUpdateFile.Close()
	oldExecutableFile.Close()

	defer func() {
		if _, err := os.Stat(firstUpdatePath); err == nil {
			os.Remove(firstUpdatePath)
		}
		if _, err := os.Stat(oldExecutablePath); err == nil {
			os.Remove(oldExecutablePath)
		}
		if _, err := os.Stat(fmt.Sprintf("%s.old", oldExecutablePath)); err == nil {
			os.Remove(fmt.Sprintf("%s.old", oldExecutablePath))
		}
	}()

	testUpdate := &Update{
		OriginalExecPath: oldExecutablePath,
		execLocalPath:    firstUpdatePath,
	}

	t.Run("when {oldExecutablePath}.old doesn't already exist", func(t *testing.T) {
		err := testUpdate.PerformRenaming()
		if err != nil {
			t.Error(err)
		}

		want := fmt.Sprintf(fmt.Sprintf("%s.old", oldExecutablePath))

		if _, err := os.Stat(want); os.IsNotExist(err) {
			t.Errorf("Expected file %s to have been renamed to %s", oldExecutablePath, want)
		}

		if _, err := os.Stat(firstUpdatePath); err == nil {
			t.Errorf("Expected file %s to have been renamed to %s", firstUpdatePath, oldExecutablePath)
		}

	})

	t.Run("when {oldExecutablePath}.old already exists", func(t *testing.T) {
		secondUpdatePath := "test_file3"
		secondUpdateFile, _ := os.Create(secondUpdatePath)
		secondUpdateFile.WriteString("SECOND UPDATE")
		secondUpdateFile.Close()

		testUpdate.execLocalPath = secondUpdatePath

		defer os.Remove(secondUpdatePath)

		err := testUpdate.PerformRenaming()
		if err != nil {
			t.Error(err)
		}

		if _, err := os.Stat(secondUpdatePath); err == nil {
			t.Errorf("Expected %s to have been renamed", secondUpdatePath)
		}
	})

}
