package main

import (
	"fmt"
	"log"
	"os"
	"strconv"
)

/*
AppStatus maintains the current status of the application. Handlers in main
use it to display current/newer versions. Stores pointer to update object
when available
*/
type AppStatus struct {
	// Stores the path for the current app instance's executable
	ExecutablePath string

	// The version of the currently running executable
	CurrentVersion string

	// Stores latest version, set after calling CheckForUpdate()
	NewVersion string

	// Update stores a pointer to an Update object. Set after calling
	// CheckForUpdate(). Contains info about remote path, local path
	// and is used to execute the update
	Update *Update
}

func NewAppStatus(currentVersion string) (*AppStatus, error) {
	executablePath, err := os.Executable()
	if err != nil {
		err = fmt.Errorf("os.Executable(): %v", err)
		return nil, err
	}
	return &AppStatus{
		CurrentVersion: currentVersion,
		NewVersion:     "",
		ExecutablePath: executablePath,
		Update:         nil,
	}, nil
}

// IsUpdateAvailable checks if NewVersion is set
func (s *AppStatus) IsUpdateAvailable() bool {
	return s.NewVersion != ""
}

/*
CheckForUpdate calls CallUpdateService to get the latest version string and
download paths for update file and signature
It compares current version to version string obtained from CallUpdateService
and if found newer, updates NewVersion assigns a new update to s.Update
*/
func (s *AppStatus) CheckForUpdate() (err error) {
	checkUpdateResponse, err := GetLatestVersionService(GetLatestVersionURL)
	if err != nil {
		err = fmt.Errorf("From CallUpdateService: %v", err)
		return
	}

	// Create a new update object with values returned from GetLatesVersionService
	s.SetUpdate(NewUpdate(
		checkUpdateResponse.Version, s.ExecutablePath, checkUpdateResponse.ExecDownloadPath,
		checkUpdateResponse.SigDownloadPath, PublicKeyPath,
	))
	return
}

// SetUpdate sets s.Update only if the update is newer than:
// 	1. The current version
//	2. The already existing update
func (s *AppStatus) SetUpdate(newUpdate *Update) (err error) {
	// Is the new update's version greater than currentVersion?
	if res, _ := versionCompare(newUpdate.Version, s.CurrentVersion); res == 1 {
		// Was an update object already set?
		if s.NewVersion != "" {
			// Is the new update's version greater than already set update's version?
			if res, _ := versionCompare(newUpdate.Version, s.NewVersion); res == 1 {
				s.NewVersion = newUpdate.Version
				s.Update = newUpdate
				return nil
			}
		} else {
			s.NewVersion = newUpdate.Version
			s.Update = newUpdate
			return nil
		}
	}
	return nil
}

// versionCompare implements custom version comparison (if version string is of a non-standard
// form like v.1.3.2) and returns:
// 0 if version1 == version2
// 1 if version1 > version2
// -1 if version1 < version2
func versionCompare(version1, version2 string) (int, error) {
	version1Float, err := strconv.ParseFloat(version1, 32)
	if err != nil {
		return 0, fmt.Errorf("Failed to convert %s to comparable value: %v", version1, err)
	}

	version2Float, err := strconv.ParseFloat(version2, 32)
	if err != nil {
		return 0, fmt.Errorf("Failed to convert %s to comparable value: %v", version2, err)
	}

	if version1Float > version2Float {
		return 1, nil
	} else if version1Float < version2Float {
		return -1, nil
	} else {
		return 0, nil
	}
}

/*
ApplyUpdate calls Update.Execute() and issues restart command
if successful. If not successful, calls Update.CleanUp() to clean up
any generated files
*/
func (s *AppStatus) ApplyUpdate(restartChannel chan<- int) {
	err := s.Update.Execute(s.ExecutablePath)
	defer s.Update.CleanUp() // Cleanup whether or not update was successful
	if err != nil {
		log.Println("Error executing update: ", err)
		return
	}

	restartChannel <- 1
}
