package main

import (
	"os"
	"testing"
)

func TestVersionCompare(t *testing.T) {

	want := -1
	got, err := versionCompare("1.1", "1.2")
	if err != nil {
		t.Fatal(err)
	}

	if got != want {
		t.Errorf("Expected %d, got %d", want, got)
	}

	want = 0
	got, err = versionCompare("1.2", "1.2")
	if err != nil {
		t.Fatal(err)
	}

	if got != want {
		t.Errorf("Expected %d, got %d", want, got)
	}

	want = 1
	got, err = versionCompare("1.3", "1.2")
	if err != nil {
		t.Fatal(err)
	}

	if got != want {
		t.Errorf("Expected %d, got %d", want, got)
	}

}

func TestSetUpdate(t *testing.T) {

	t.Run("when newVersion isn't set (no existing update object)", func(t *testing.T) {

		t.Run("when newUpdate version > currentVersion", func(t *testing.T) {
			testAppStatus := &AppStatus{CurrentVersion: "1.1"}
			newUpdate := &Update{Version: "1.2"}

			testAppStatus.SetUpdate(newUpdate)
			got := testAppStatus.NewVersion
			want := newUpdate.Version
			if got != want {
				t.Errorf("Expected %s got %s", want, got)
			}
			if testAppStatus.Update == nil {
				t.Errorf("Expected testAppStatus.Update to be set")
			}
		})

		t.Run("when newUpdate version < currentVersion", func(t *testing.T) {
			testAppStatus := &AppStatus{CurrentVersion: "1.1"}
			newUpdate := &Update{Version: "1.0"}

			testAppStatus.SetUpdate(newUpdate)
			if testAppStatus.NewVersion != "" {
				t.Errorf("Expected NewVersion not to be set")
			}
			if testAppStatus.Update != nil {
				t.Errorf("Expected Update not to be set")
			}
		})
	})

	t.Run("when newVersion & Update has already been set previously", func(t *testing.T) {
		oldUpdate := &Update{Version: "1.2"}

		testAppStatus := &AppStatus{
			CurrentVersion: "1.1",
			NewVersion:     "1.2",
			Update:         oldUpdate,
		}

		t.Run("when newUpdate version is same as old update's version", func(t *testing.T) {
			newUpdate := &Update{Version: "1.2"}

			testAppStatus.SetUpdate(newUpdate)

			want := oldUpdate.Version
			got := testAppStatus.NewVersion

			if got != want {
				t.Errorf("Expected %s got %s", want, got)
			}

			wantUpdate := oldUpdate
			gotUpdate := testAppStatus.Update

			if gotUpdate != wantUpdate {
				t.Errorf("Expected newUpdate, got oldUpdate")
			}
		})

		t.Run("when newUpdate version is greater than existing update's version", func(t *testing.T) {
			newUpdate := &Update{Version: "1.3"}
			testAppStatus.SetUpdate(newUpdate)

			want := newUpdate.Version
			got := testAppStatus.NewVersion

			if got != want {
				t.Errorf("Expected %s got %s", want, got)
			}

			wantUpdate := newUpdate
			gotUpdate := testAppStatus.Update

			if gotUpdate != wantUpdate {
				t.Errorf("Expected newUpdate, got oldUpdate")
			}
		})

	})
}

func TestCheckForUpdate(t *testing.T) {
	testVersion := "1.8"
	testExecRemotePath := "www.google.com"
	testSigRemotePath := "www.twitter.com"

	serviceFilePath := "mock_service\\version_service"
	os.Mkdir("mock_service", 0777)
	f, err := os.Create(serviceFilePath)
	if err != nil {
		t.Error(err)
	}
	f.WriteString(`
		{
			"version": "1.8",
			"executable": "www.google.com",
			"signature": "www.twitter.com"
		}
	`)
	f.Close()
	defer os.RemoveAll("mock_service")

	testAppStatus := &AppStatus{
		CurrentVersion: "1.1",
	}

	if err := testAppStatus.CheckForUpdate(); err != nil {
		t.Error(err)
	}

	want := testVersion
	got := testAppStatus.NewVersion

	if want != got {
		t.Errorf("Expected %s got %s", want, got)
	}

	want = testVersion
	got = testAppStatus.Update.Version

	if want != got {
		t.Errorf("Expected %s got %s", want, got)
	}

	if testAppStatus.Update.execRemotePath != testExecRemotePath {
		t.Errorf("Expected %s got %s", testExecRemotePath, testAppStatus.Update.execRemotePath)
	}

	if testAppStatus.Update.sigRemotePath != testSigRemotePath {
		t.Errorf("Expected %s got %s", testSigRemotePath, testAppStatus.Update.sigRemotePath)
	}

}
