package main

import (
	"bufio"
	"crypto"
	"crypto/rsa"
	"crypto/sha256"
	"crypto/x509"
	"encoding/base64"
	"encoding/pem"
	"fmt"
	"io"
	"io/ioutil"
	"os"
)

// MaxSignLengthBytes stores the maximum number of bytes to read from the signature file.
// Since we load signature file into memory, a large file may crash the program. PKCS1v15 signatures for RSA 2048 keys
// with sha256 hash stored as base64 are 344 bytes in length
const MaxSignLengthBytes = 400

/*
Update handles:
1. Storing of remote/local paths for update's executable & signature
2. Downloading of executable & signature from remote to local
3. Verification of downloaded executable's signature
4. Applying update
5. Cleaning up update after success
*/
type Update struct {
	// The version number for the update available on web-service
	Version string

	// The running executable's path. Updated file name is set to this by PerformRenaming()
	OriginalExecPath string

	// The remote paths for the update executable & signature
	execRemotePath string
	sigRemotePath  string

	// The local path for the new update (set by DownloadFiles())
	execLocalPath string

	// execHashSum contains the update executable's calculated sha256sum. Set by LoadFileHash()
	execHashSum []byte

	// The local path for the new update's signature (set by DownloadFiles())
	sigLocalPath string

	// The decoded signature from file at sigLocalPath (set by LoadSignature())
	Signature []byte

	// The public key to use for verifying update's signature. Loaded by
	// LoadPlublicKey() during execution
	PublicKey *rsa.PublicKey

	// The path to the public key, set at initialization
	PubKeyPath string
}

/*
NewUpdate returns a pointer to Update object with provided
version & remote path
*/
func NewUpdate(version, origPath, execRemotePath, sigRemotePath, pubKeyPath string) (update *Update) {
	update = &Update{
		Version:          version,
		OriginalExecPath: origPath,
		execRemotePath:   execRemotePath,
		sigRemotePath:    sigRemotePath,
		PubKeyPath:       pubKeyPath,
	}

	return
}

/*
Execute downloads the update files from the remote paths to local paths.
It verifies the file's signature and if OK:
	1. 	Renames the currently running executable
	2.	Renames the downloaded file to the currently running executable's name (originalExecPath)
*/
func (update *Update) Execute(originalExecPath string) (err error) {

	if err = update.DownloadFiles(); err != nil {
		return fmt.Errorf("From DownloadFiles(): %v", err)
	}

	if err = update.LoadFileHash(); err != nil {
		return fmt.Errorf("From LoadFileHash(): %v", err)
	}

	if err = update.LoadPublicKey(); err != nil {
		return fmt.Errorf("From LoadPublicKey(): %v", err)
	}

	if err = update.LoadSignature(); err != nil {
		return fmt.Errorf("From LoadSignature(): %v", err)
	}

	if err = rsa.VerifyPKCS1v15(update.PublicKey, crypto.SHA256, update.execHashSum, update.Signature); err != nil {
		return fmt.Errorf("From VerifyPKCS1v15(): %v", err)
	}

	if err = update.PerformRenaming(); err != nil {
		return fmt.Errorf("From PerformRenaming(): %v", err)
	}

	return
}

// PerformRenaming handles renaming of downloaded update & old executable
func (update *Update) PerformRenaming() error {
	// The currently running executable is renamed to "{originalExecPath}.old"
	oldFileRenameTo := fmt.Sprintf("%s.old", update.OriginalExecPath)

	if doesFileExist(oldFileRenameTo) {
		// If the app was previously updated, there would already exist a file named {originalExecPath}.old
		// Clean up this old file
		if err := os.Remove(oldFileRenameTo); err != nil {
			return fmt.Errorf("Error removing outdated executable %s: %v", oldFileRenameTo, err)
		}
	}

	// Rename the old executable to oldFileRenameTo, guaranteed not to exist
	err := os.Rename(update.OriginalExecPath, oldFileRenameTo)
	if err != nil {
		return fmt.Errorf("Error renaming old executable %s to %s: %v", update.OriginalExecPath, oldFileRenameTo, err)
	}

	// Rename downloaded update to required executable name
	err = os.Rename(update.execLocalPath, update.OriginalExecPath)
	if err != nil {
		return fmt.Errorf("Error renaming new executable %s to %s: %v", update.execLocalPath, update.OriginalExecPath, err)
		// TO-DO
	}
	return nil
}

// DownloadFiles downloads update & signature from update.execRemotePath & update.sigRemotePath and sets
// their local paths in execLocalPath & sigLocalPath
func (update *Update) DownloadFiles() error {
	// Download the executable to {originalExecPath}.new
	execDestPath := fmt.Sprintf("%s.new", update.OriginalExecPath)
	err := DownloadFileService(update.execRemotePath, execDestPath)
	if err != nil {
		return fmt.Errorf("Error from DownloadFileService() while downloading %s: %v", update.execRemotePath, err)
	}
	update.execLocalPath = execDestPath

	// Download the signature to {originalExecPath}.new.sign
	signDestPath := fmt.Sprintf("%s.sign", execDestPath)
	err = DownloadFileService(update.sigRemotePath, signDestPath)
	if err != nil {
		return fmt.Errorf("Error from DownloadFileService() while downloading %s: %v", update.sigRemotePath, err)
	}
	update.sigLocalPath = signDestPath
	return nil
}

// LoadFileHash reads file at execLocalPath, calculates it's sha256sum and saves
// in update.execHashSum
func (update *Update) LoadFileHash() error {
	verifyFile, err := os.Open(update.execLocalPath)
	if err != nil {
		return fmt.Errorf("Error opening %s: %v", update.execLocalPath, err)
	}
	defer verifyFile.Close()

	verifyFileHash := sha256.New()
	if _, err = bufio.NewReader(verifyFile).WriteTo(verifyFileHash); err != nil {
		return fmt.Errorf("Error writing hash for %s: %v", update.execLocalPath, err)
	}
	update.execHashSum = verifyFileHash.Sum(nil)
	return nil
}

func (update *Update) LoadPublicKey() error {
	keyFile, err := os.Open(update.PubKeyPath)
	if err != nil {
		return fmt.Errorf("Error opening %s: %v", update.PubKeyPath, err)
	}
	defer keyFile.Close()

	pubKey, err := loadPublicKeyFrom(keyFile)
	if err != nil {
		return fmt.Errorf("From LoadPublicKey(): %v", err)
	}
	update.PublicKey = pubKey
	return nil
}

func (update *Update) LoadSignature() error {
	signFile, err := os.Open(update.sigLocalPath)
	if err != nil {
		return fmt.Errorf("Error opening %s: %v", update.sigLocalPath, err)
	}
	defer signFile.Close()

	signatureBytes, err := getBase64DecodedBytes(signFile, MaxSignLengthBytes)
	if err != nil {
		return fmt.Errorf("Error reading base64 sign from file %s: %v", update.sigLocalPath, err)
	}

	update.Signature = signatureBytes
	return nil
}

/*
CleanUp deletes the following generated files:
1. .execLocalPath if exists
2. .sigLocalPath if exists
*/
func (update *Update) CleanUp() (err error) {
	// Check whether update was ever downloaded
	if (update.execLocalPath != "") || (update.sigLocalPath != "") {
		if doesFileExist(update.execLocalPath) {
			err = os.Remove(update.execLocalPath)
			if err != nil {
				return err
			}
		}
		if doesFileExist(update.sigLocalPath) {
			err = os.Remove(update.sigLocalPath)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func doesFileExist(path string) bool {
	_, err := os.Stat(path)
	if err == nil {
		return true
	} else if os.IsNotExist(err) {
		return false
	} else {
		return false
	}
}

// getBase64Sign reads from signReader & returns base64 std decoded []byte
func getBase64DecodedBytes(signReader io.Reader, maxBytes int) (signBytes []byte, err error) {
	base64bytes := make([]byte, maxBytes)
	bytesRead, err := signReader.Read(base64bytes)
	if err != nil {
		return nil, err
	}

	signBytes, err = base64.StdEncoding.DecodeString(string(base64bytes[:bytesRead]))
	if err != nil {
		return nil, err
	}
	return signBytes, nil
}

func loadPublicKeyFrom(keyFile io.Reader) (*rsa.PublicKey, error) {

	b, err := ioutil.ReadAll(keyFile)
	if err != nil {
		return nil, err
	}

	derBlock, _ := pem.Decode(b)

	pubKey, err := x509.ParsePKCS1PublicKey(derBlock.Bytes)
	if err != nil {
		return nil, err
	}

	return pubKey, nil
}
