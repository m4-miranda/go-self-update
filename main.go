package main

import (
	"html/template"
	"log"
	"net/http"
	"os"
	"os/exec"
	"os/signal"
	"syscall"
)

const (
	// Version indicates the current version for this source code
	Version = "1.8"

	// PublicKeyPath indicates the signer's public key used to verify
	// update's signature. Used in AppStatus.CheckForUpdate()
	PublicKeyPath = "pub.pem"
)

// CheckHandler handles requests to /check
// Calls AppStatus.CheckForUpdate() & redirects to "/"
func CheckHandler(currentStatus *AppStatus) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		err := currentStatus.CheckForUpdate()
		if err != nil {
			log.Println("Error from CheckForUpdate(): ", err)
		}

		http.Redirect(w, r, "/", 303)
	}
}

// InstallHandler handles requests to /install
func InstallHandler(
	restartChannel chan<- int, currentStatus *AppStatus,
	updatePage *template.Template,
) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if currentStatus.IsUpdateAvailable() {
			if err := updatePage.Execute(w, currentStatus); err != nil {
				log.Println("Error from InstallHandler - updatePage.Execute(): ", err)
				return
			}
			go currentStatus.ApplyUpdate(restartChannel)
		} else {
			// Redirect to home page if no update possible
			http.Redirect(w, r, "/", 303)
		}
	}
}

func main() {
	f, err := os.OpenFile("go-self-update.log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Fatal("Error from Main(): opening file: ", err)
	}
	defer f.Close()

	log.SetOutput(f)

	log.Println("\n\nStarted go-self-update version: ", Version)

	restartChannel := make(chan int)

	// Maintain a *http.Server object so we can cleanly shut it down during updates
	s := &http.Server{Addr: ":8080"}

	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	currentStatus, err := NewAppStatus(Version)
	if err != nil {
		log.Fatal("Error from NewAppStatus: ", err)
	}

	page, err := template.New("page").Parse(homePageTemplate)
	if err != nil {
		log.Fatal(err)
	}

	updatePage, err := template.New("update-page").Parse(updatePageTemplate)
	if err != nil {
		log.Fatal(err)
	}

	// main page> show version and update check
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		if err := page.Execute(w, currentStatus); err != nil {
			log.Println("Error from handler '/': ", err)
		}
	})

	http.HandleFunc("/check", CheckHandler(currentStatus))
	http.HandleFunc("/install", InstallHandler(restartChannel, currentStatus, updatePage))

	log.Println("Starting server...")
	// ListenAndServe HTTP server from a go-routine so that shutting it down
	// doesn't end the main program
	go func() {
		s.ListenAndServe()
	}()

	// Go-routine handling restarting of the app when message received on
	// restartChannel
	go func() {
		// Wait for message to restart
		<-restartChannel
		log.Println("Restart command received. Shutting down HTTP server...")

		// Gracefully shutdown http server
		s.Shutdown(nil)

		log.Printf("Launching new program version %s ...\n", currentStatus.NewVersion)
		// Launch the new executable
		cmd := exec.Command(currentStatus.ExecutablePath)
		cmd.Start()

		log.Printf("Exiting old program version %s...\n", currentStatus.CurrentVersion)
		// Send interrup to main() to end program
		interrupt <- os.Interrupt
	}()

	<-interrupt
}
