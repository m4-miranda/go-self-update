package main

var (
	homePageTemplate = `<!DOCTYPE html>
									<html>
										<head>
											<title> Server {{.CurrentVersion}} </title>
										</head>
										<body>
											<h1>This server has {{.CurrentVersion}}</h1>
											<a href="check">Check version</a>
											<br>
											{{if .NewVersion}}New version is available: {{.NewVersion}} | <a
											href="install">Download and install</a>{{end}}
										</body>
									</html>`

	updatePageTemplate = `<!DOCTYPE html>
						  <html>
							  <head>
							  	<title>Server Update</title>
							  </head>
							  <body>
								<h1>Server is updating to version {{.NewVersion}}...</h1>
								<br>
								<a href="/">Click here to go back home</a>
							  </body>
						  </html>`
)
