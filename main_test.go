package main

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
)

const (
	testUpdateVersion = "1.9"
	testExecPath      = ".\\mock_service\\test-update.exe"
	testSignPath      = ".\\mock_service\\test-sign"
)

func CreateDirectoryStructure() {
	os.Mkdir("mock_service", 0777)
	f, _ := os.Create("mock_service\\version_service")
	f1, _ := os.Create(testExecPath)
	f2, _ := os.Create(testSignPath)

	f.WriteString(fmt.Sprintf(
		`{
			"version": "%s",
			"executable": "%s",
			"signature": "%s"
		}`,
		"1.9", testExecPath, testSignPath))
	f1.WriteString("TEST FILE")
	f2.WriteString("TEST FILE 2")

	f.Close()
	f1.Close()
	f2.Close()
}

func TestCheckHandler(t *testing.T) {
	testAppStatus := &AppStatus{}

	CreateDirectoryStructure()
	defer os.RemoveAll("mock_service")

	t.Run("expect redirect", func(t *testing.T) {
		request := httptest.NewRequest(http.MethodGet, "/check", nil)

		responseRecorder := httptest.NewRecorder()

		httpHandler := CheckHandler(testAppStatus)

		httpHandler(responseRecorder, request)

		resp := responseRecorder.Result()
		if resp.StatusCode != 303 {
			t.Errorf("Expected status code %d, got %d", 303, resp.StatusCode)
		}
	})
}

func TestInstallHandler(t *testing.T) {

	t.Run("when no update is set", func(t *testing.T) {
		testAppStatus := &AppStatus{
			CurrentVersion: "1.1",
			NewVersion:     "",
		}

		request := httptest.NewRequest(http.MethodGet, "/install", nil)

		rr := httptest.NewRecorder()

		httpHandler := InstallHandler(make(chan int, 1), testAppStatus, nil)

		httpHandler(rr, request)

		resp := rr.Result()
		if resp.StatusCode != 303 {
			t.Errorf("Expected status code %d, got %d", 303, resp.StatusCode)
		}
	})

}
