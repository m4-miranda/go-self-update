# go-self-update

## Output directory 
--------------------

* `go-self-update.exe`

    The executable that must be run to test functionality. Sample output is built with version `1.2`

* `pub.pem`

    The updating authority's RSA public key in PEM format

* `go-self-update.log`

    Log file for go-self-update

* `mock_service\`

    Directory that houses a `update.json`, new executable & signature files. This directory mocks an external web service that provides info on software updates available 

    * `update.json`

        Contains 3 keys: 

            1. `version`: The version for the latest executable 

            2. `executable`: The external URL for downloading the update (relative to `go-self-update.exe`)

            3. `signature`: The external URL for downloading update file's signature (relative to `go-self-update.exe`)

    * `update-x.x.exe`

        Actual updated exe that is downloaded (copied) to the parent directory during an update

    * `update-x.x.exe.sign`

        Update file's signature signed by trusted private key

## Source Code
--------------------

#### Build Instructions

* Make following directory structure:

    ```
    output\
        |
        |_ mock_service\
    ```

* Generate RSA key pair:

    * Provided `sign.exe` includes helper commands to generate RSA pub/priv key pair & sign/verify files (https://bitbucket.org/m4-miranda/go-rsa-sign/overview)

    * To generate public/private key pair and save to file in PEM format:

        `sign.exe --keys-only priv.pem pub.pem`

    * Copy `pub.pem` to `.\output\`. This public key will be used by the program to verify the available update's signature

* Building lower version executable:

    * Set `Version` const in `main.go` as **DECIMAL** string, e.g. `Version = "1.2"`

    * Run `go build -o .\output\go-self-update.exe`

* Build & sign higher version executable:

    * Set `Version` const in `main.go` to higher decimal value

    * Run `go build -o .\output\mock_service\update-X.x.exe`

    * Run `sign.exe -k priv.pem -f .\output\mock_service\update-X.x.exe -o .\output\mock_service\update-X.x.exe.sign`

        * Signs the generated executable `update-X.x.exe` using RSA private key `priv.pem` (generated earlier) and outputs the signature in base64 format into `update-X.x.exe.sign`

    * Create `update.json` file in `output\mock_service\` as:

        ```json
        {
            "version": "X.x",
            "executable": ".\mock_service\update-X.x.exe",
            "signature":".\mock_service\update-X.x.exe.sign"
        }
        ```

* Final directory structure:

    ```
    output\
        |-- go-self-update.exe
        |-- pub.pem
        |-- go-self-update.log**
        |__ mock_service\
                |__  update.json
                |__  update-X.x.exe 
                |__  update-X.x.exe.sign
    ```

    **Will be generated at first-run by go-self-update.exe

* Run `cd output`  `.\go-self-update.exe` and access page on `localhost:8080`

#### Testing

* To run all tests: `go test`

    P.S. Testing of /install handler requires update object to be set & calls AppStatus.ApplyUpdate() as a go-routine. Some errors are thrown since the files may not exist, but tests pass nonetheless.